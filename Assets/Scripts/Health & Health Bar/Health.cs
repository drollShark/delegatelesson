using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int _health = 3;
    public int PlayerHelath => _health > 0 ? _health : 3;

    private HealthBar healthBarScript;

    public delegate void OnDeath();

    public static event OnDeath onDeath;

    private void OnEnable()
    {
        ObstacleDamage.onDamage += ObstacleDamage_onDamage;
    }

    private void OnDisable()
    {
        ObstacleDamage.onDamage -= ObstacleDamage_onDamage;
    }

    private void Start()
    {
        _health = PlayerHelath;

        healthBarScript = FindObjectOfType(typeof(HealthBar)) as HealthBar;
        healthBarScript.SetMaxHealth(_health);
    }

    private void ObstacleDamage_onDamage(int damage)
    {
        if (_health > 1)
        {
            _health -= damage;
            healthBarScript.SetHealth(_health);
        }
        else if (_health <= 1)
        {
            onDeath?.Invoke();
            healthBarScript.SetHealth(0);
        }
    }


}
