using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Components

    private Rigidbody playerRb;
    private Animator playerAnim;

    private AudioSource playerAudio;
    public AudioClip JumpSound;
    public AudioClip CrashSound;

    #endregion

    #region Serialize

    [SerializeField]
    private float _gravityModifier = 2.0f;

    public float GravityModifier => _gravityModifier > 0 ? _gravityModifier : 2.0f;

    [SerializeField]
    private float _jumpForce = 700.0f;

    public float JumpForce => _jumpForce > 0 ? _jumpForce : 700.0f;

    private bool _gameOver = false;

    public bool GameOver => _gameOver;

    private bool isOnGround = true;

    #endregion

    #region Event and Delegate
    public static event OnGround onGround;
    public delegate void OnGround(bool isOnGround);
    #endregion

    private void Start() 
    {
        playerRb = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();

        _gravityModifier = GravityModifier;
        _jumpForce = JumpForce;
        Physics.gravity *= _gravityModifier;
    }

    private void OnEnable()
    {
        Health.onDeath += Health_onDeath;
    }

    private void OnDisable()
    {
        Health.onDeath -= Health_onDeath;
    }

    private void Health_onDeath()
    {
        _gameOver = true;
        playerAnim.SetBool("Death_b", true);
        playerAnim.SetInteger("DeathType_int", 1);;
    }

    private void Update()
    {
        Jump();
    }

   private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround == true && _gameOver == false)
        {
            isOnGround = false;
            onGround?.Invoke(isOnGround);

            playerRb.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);

            playerAnim.SetTrigger("Jump_trig");

            playerAudio.PlayOneShot(JumpSound, 1.0f);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            onGround?.Invoke(isOnGround);
        }
    }
}
