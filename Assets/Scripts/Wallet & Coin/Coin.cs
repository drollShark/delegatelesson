using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField]
    private int _coiinValue = 1;
    public int CoinValue => _coiinValue >= 0 ? _coiinValue : 1;

    public static event OnCoinPickUp coinPicUp;
    public delegate void OnCoinPickUp(int coinValue);

    private void Start()
    {
        _coiinValue = CoinValue;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            coinPicUp?.Invoke(_coiinValue);
            Destroy(gameObject);
        }
    }

}
