using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wallet : MonoBehaviour
{
    private Text coinTextComponent;

    [SerializeField]
    private int _walletCount;
    public int WalletCount => _walletCount >= 0 ? _walletCount : 0;

    private void OnEnable()
    {
        Coin.coinPicUp += Coin_coinPicUp;
    }

    private void OnDisable()
    {
        Coin.coinPicUp -= Coin_coinPicUp;
    }

    private void Start()
    {
        _walletCount = WalletCount;
        coinTextComponent = GetComponent<Text>();
        coinTextComponent.text = $"You pickUp: {_walletCount}";
    }

    private void Coin_coinPicUp(int coinValue)
    {
        _walletCount += coinValue;
        coinTextComponent.text = $"You pickUp: {_walletCount}";
    }


}
