using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    public GameObject[] _obstaclePrefabs;
    public GameObject coinPrefabs;

    [SerializeField]
    private float _startDelay = 2.0f, _repeatReat = 1.5f;

    public float StartDelayTime => _startDelay > 0 ? _startDelay : 2.0f;
    public float RepeatReatTime => _repeatReat > 0 ? _repeatReat : 1.5f;

    private PlayerController playerControllerScript;

    private void OnEnable()
    {
        Health.onDeath += Health_onDeath;
    }

    private void OnDisable()
    {
        Health.onDeath += Health_onDeath;
    }
    private void Health_onDeath()
    {
        CancelInvoke();
    }


    private void Start()
    {
        _startDelay = StartDelayTime;
        _repeatReat = RepeatReatTime;

        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        InvokeRepeating("SpawnObstacles", _startDelay, _repeatReat);
        InvokeRepeating("SpawnCoin", _startDelay, _repeatReat);
    }

    void SpawnObstacles()
    {
        Vector3 spawnLocation = new Vector3(30, 0, 0);
        int index = Random.Range(0, _obstaclePrefabs.Length);

        Instantiate(_obstaclePrefabs[index], spawnLocation, _obstaclePrefabs[index].transform.rotation, transform.parent);
    }

    void SpawnCoin()
    {
        Vector3 spawnLocation = new Vector3(30, Random.Range(3f, 5), 0);

        Instantiate(coinPrefabs, spawnLocation, transform.rotation, transform.parent);
    }
}
