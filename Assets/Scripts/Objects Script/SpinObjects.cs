﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinObjects : MonoBehaviour
{
    [SerializeField]
    private float _spinSpeed = 250;
    public float SpinSpeed => _spinSpeed > 0 ? _spinSpeed : 250;

    void Update()
    {   
        transform.Rotate(Vector3.up, _spinSpeed * Time.deltaTime, Space.Self);
    }
}
