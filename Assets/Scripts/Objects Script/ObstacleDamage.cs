using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDamage : MonoBehaviour
{
    [SerializeField]
    private int _obstacleDmageValue = 1;
    public int ObstacleDamageValue => _obstacleDmageValue > 0 ? _obstacleDmageValue : 1;

    public static event OnObstacleDamage onDamage;
    public delegate void OnObstacleDamage(int damage);

    private void Start()
    {
        _obstacleDmageValue = ObstacleDamageValue;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            onDamage?.Invoke(_obstacleDmageValue);
        }
    }
}
