using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    [SerializeField]
    private float _speed = 10.0f, _leftBound = 11.0f;
    public float SpeedMoveLeft => _speed > 0 ? _speed : 10.0f;
    public float LeftBoundRange => _leftBound;

    private bool gameOver = false;

    private void OnEnable()
    {
        Health.onDeath += Health_onDeath;
    }


    private void OnDisable()
    {
        Health.onDeath -= Health_onDeath;
    }

    private void Health_onDeath()
    {
        gameOver = true;
    }
       
    private void Start()
    {
        _speed = SpeedMoveLeft;
        _leftBound = LeftBoundRange;
    }

    private void Update()
    {
        MoveToLeftDirection();
    }

    void MoveToLeftDirection()
    {
        if (gameOver == false)
        {
            transform.Translate(Vector3.left * Time.deltaTime * _speed, Space.World);
        }

        if (transform.position.x < -_leftBound && gameObject.CompareTag("Obstacle") || transform.position.x < -_leftBound && gameObject.CompareTag("Money"))
        {
            Destroy(gameObject);
        }
    }
}
