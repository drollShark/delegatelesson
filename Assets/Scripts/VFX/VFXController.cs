using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXController : MonoBehaviour
{
    public ParticleSystem ExplousionParticle;
    public ParticleSystem DirtParticle;

    private void OnEnable()
    {
        PlayerController.onGround += PlayerController_onGround;
        Health.onDeath += Health_onDeath;
    }

    private void OnDisable()
    {
        PlayerController.onGround -= PlayerController_onGround;
        Health.onDeath -= Health_onDeath;
    }
    private void PlayerController_onGround(bool _isOnGround)
    {
        if (_isOnGround == false)
        {
            DirtParticle.Stop();
        }
        else
        {
            DirtParticle.Play();
        }
    }

    private void Health_onDeath()
    {
        DirtParticle.Stop();
        ExplousionParticle.Play();
    }
}
